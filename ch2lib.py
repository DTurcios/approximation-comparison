#Chapter 2 Library

import math
import numpy as np
import matplotlib.pyplot as plt

# Euler method
# a: initial value of independent variable
# b: final value of independent variable
# y0: initial value of dependent variable
# n : number of steps (higher the better)
# Returns values within array of t and y

def Euler(f, a, b, y0, n):
    print(f.__name__)
    #use n to create intervals
    h = (b - a)/(n)
    print("h = {}".format(h))
    
    #increase n to make the correct amount of points
    n += 1
    #create empty arrays
    t = np.zeros([n])
    y = np.zeros([n])
    
    #set first value for arrays 
    t[0]= a
    y[0]= y0
    for i in range(1, n):
        #y(i) = y(i-1) + h*f(t,y)
        yn = y[i-1] + h*f(t[i - 1],y[i - 1])
        y[i] = yn
        #add step size to t to get next t value
        t[i] = t[i-1] + h
    #print vertex information
    #for i in range(n):
        #print("t_{2:2}: {0:6}, y_{2:2}: {1}".format(t[i],y[i], i))     
    
    #return arrays t and y
    return t, y

def Heun(f, a, b, y0, n):
    
    #create intervals
    h = (b - a)/(n)
    n += 1
    
    #create empty arrays
    t = np.zeros([n])
    y = np.zeros([n])
    
    #set first positions
    t[0]= a
    y[0]= y0
    
    #perform Heuns
    for i in range(1, n):
        
        y_temp = y[i-1] + h*f(t[i-1],y[i-1])
        
        #increase t here because it is needed
        t[i] = t[i-1] + h
        
        yn = y[i-1]+(h/2.0)*(f(t[i-1], y[i-1]) + f(t[i], y_temp))
        y[i] = yn
        
    return t, y

def rk4(f, a, b, y0, n):
    
    #empty x/y arrays
    vt = np.zeros([n+1])
    vy = np.zeros([n+1])
    
    #create the step size
    h = (b - a) / float(n)
    
    #set x/y and the first x/y array spot
    vt[0] = t = a
    vy[0] = y = y0
    
    for i in range(1, n + 1):
        
        #k1 = h*f(t_n,y_n)
        k1 = h * f(t, y)
        
        #k2 = h*f(t_n + h/2, y_n + k1/2)
        k2 = h * f(t + h/2, y + k1/2)
        
        #k3 = h*f(t_n + h/2, y_n + k1/2)
        k3 = h * f(t + h/2, y + k2/2)
        
        #k4 = h*f(t_n + h/2, y_n + k3)
        k4 = h * f(t + h, y + k3)
        
        #increase t
        t = a + i * h
        vt[i] = t
        
        y = y + (1/6)*(k1 + k2 + k2 + k3 + k3 + k4)
        vy[i] = y
    return vt, vy
 


def plotEuler(t, y):
    plt.plot(t, y, 'b.-')
    plt.xlabel('t')
    plt.ylabel('y')
    plt.title("Approximation")
    plt.show()

def plotGraph(t, y):
    plt.plot(t, y, 'k', linewidth = 0.5)    
    plt.show()
    
def plotError(t, eulerError, huenError, rk4Error):
    
    plt.plot(t, eulerError, 'bo-', label = 'Euler')
    plt.plot(t, huenError, 'yo-', label = 'Heun')
    plt.plot(t, rk4Error, 'go-', label = 'rk4')
    plt.xlabel('t')
    plt.ylabel('y')
    plt.title("Error Comparison")
    plt.legend()
    plt.show()     

def plotComparison(et, ey, ht, hy, rk4t, rk4y, f, eError, hError, rError):
    exf = []
    length = int(len(ey))
    for i in range(length):
        exf.append(f(et[i]))
        
        print("t = {:.3f}".format(et[i]))
        print("Exact y = {:.5f}".format(exf[i]))
        print("Euler y = {:.5f},  Relative Error = {:.8e}".format(ey[i], eError[i]))
        print("Huen  y = {:.5f},  Relative Error = {:.8e}".format(hy[i], hError[i]))
        print("RK4   y = {:.5f},  Relative Error = {:.8e}".format(rk4y[i], rError[i]))
        print("\n")
        
    plt.plot(et, ey, 'bo-', label = 'Euler')
    plt.plot(ht, hy, 'yo-', label = 'Heun')
    plt.plot(rk4t, rk4y, 'go-', label = 'rk4')
    plt.plot(et, exf, 'k.-', label = 'Exact')
    plt.xlabel('t')
    plt.ylabel('y')
    plt.title("Approximation Comparison")
    plt.legend()
    plt.show() 
    
def plotExact(t, y, erf, exf):
    plt.plot(t, y, 'bo-', label = 'Approximaation')
    plt.plot(t, erf, 'ro-', label = 'Error')
    plt.plot(t, exf, 'k.-', label = 'Exact')
    plt.xlabel('t')
    plt.ylabel('y')
    plt.title("Approximation")
    plt.legend()
    plt.show() 

def exactErrorDisplay(t, y, f, n):
    #create empty array to hold error values
    erf = []
    #create empty array to hold exact values
    exf = []
    length = int(len(y))
    for i in range(length):
        temp = abs(f(t[i]) - y[i])/ abs(f(t[i]))
        #append value to array of error values
        erf.append(temp)
        #append value to array of exact function
        exf.append(f(t[i]))
        #print("t = {:.3f}".format(t[i]))
        #print("Apprx y = {:8f}".format(y[i]))
        #print("Exact y = {:8f}".format(f(t[i])))
        #print("Relative error: {:8e}\n".format(temp))
    #Plot exact and error
    if(n == 0):       
        plotExact(t, y, erf, exf)  

def findError(t, y, f):
    #create empty array to hold error values
    erf = []
    length = int(len(y))
    for i in range(length):
        temp = abs(f(t[i]) - y[i])/ abs(f(t[i]))
        #append value to array of error values
        erf.append(temp)
    return erf

if __name__ == "__main__":
    f = lambda t, y: math.sin(t) - y
    f.__name__ = 'y\'= sin(t) - y'
    a = 0
    b = 1
    y0 = 1
    
    t, y = Euler(f, a, b, y0, 4)
    plotEuler(t,y)
    
    t, y = Euler(f, a, b, y0, 8)
    plotEuler(t,y)
    
    t, y = Euler(f, a, b, y0, 16)
    plotEuler(t,y)