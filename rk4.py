#RK4 v Euler

import math
import time
import library.ch2lib as ch2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D #<-- Note the capitalization! 



f = lambda t, y : (math.sin(t) - y)
f.__name__ = 'y\'= sin(t) - y'

ef = lambda t : (- math.cos(t) + math.sin(t) - math.exp(-t))/(2)
ef.__name__ = 'y = (-cos(t) + sin(t) - e^-t)/2'

a = 0
b = 10
y0 = -1
n = 16

flag = 0

print("Euler's Approximation\n")
start = time.time()
et, ey = t, y = ch2.Euler(f, a, b, y0, n)
ch2.exactErrorDisplay(t,y,ef,flag)
eulerError = ch2.findError(t,y,ef)
end = time.time()
print("It took {} seconds to compute via Eulers. \n".format(end - start))

print("Heun's Approximation\n")
start = time.time()
ht, hy = t, y = ch2.Heun(f, a, b, y0, n)
ch2.exactErrorDisplay(t,y,ef,flag)
huenError = ch2.findError(t,y,ef)
end = time.time()
print("It took {} seconds to compute via Heuns. \n".format(end - start))

print("RK4 Approximation\n")
start = time.time()
rk4t, rk4y = t, y = ch2.rk4(f, a, b, y0, n)
ch2.exactErrorDisplay(t,y,ef,flag)
rk4Error = ch2.findError(t,y,ef)
end = time.time()
print("It took {} seconds to compute via RK4. \n".format(end - start))

print("This is all the approximations compared to each other")
ch2.plotComparison(et, ey, ht, hy, rk4t, rk4y, ef, eulerError, huenError, rk4Error)
ch2.plotError(et, eulerError, huenError, rk4Error)



#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#Lorenz Attractor
#x is the rate of convection
#y is the horizontal temperature variation
#z is the vertical temperature variation
#sigma is the Prandtl Number 28
#rho is Rayleigh number 10
#beta is a certain physical dimension of layer itself 8/3

   
#lorenz equations
def dx_dt(x, y, sigma):
    return sigma*(y - x)

def dy_dt(x, y, z, rho):
    return x*(rho - z) - y

def dz_dt(x, y, z, beta):
    return x*y - beta*z

def lz_euler(x,y,z,sigma,rho,beta,h):
    xn = x + h * dx_dt(x, y, sigma)
    yn = y + h * dy_dt(x, y, z, rho)
    zn = z + h * dz_dt(x, y, z, beta)
    
    return (xn, yn, zn)

#rk4 for Lorenz Attractor
def rk4(x, y, z, sigma, rho, beta, h):
    k1 = dx_dt(x, y, sigma)
    j1 = dy_dt(x, y, z, rho)
    m1 = dz_dt(x, y, z, beta)
    
    k2 = dx_dt((x + h*(k1/2)), (y + h*(j1/2)), sigma)
    j2 = dy_dt((x + h*(k1/2)), (y + h*(j1/2)), (z + h*(m1/2)), rho)
    m2 = dz_dt((x + h*(k1/2)), (y + h*(j1/2)), (z + h*(m1/2)), beta)

    k3 = dx_dt((x + h*(k2/2)), (y + h*(j2/2)), sigma)
    j3 = dy_dt((x + h*(k2/2)), (y + h*(j2/2)), (z + h*(m2/2)), rho)
    m3 = dz_dt((x + h*(k2/2)), (y + h*(j2/2)), (z + h*(m2/2)), beta)
    
    k4 = dx_dt((x + h*k3), (y + h*j3), sigma)
    j4 = dy_dt((x + h*k3), (y + h*j3), (z + h*m3), rho)
    m4 = dz_dt((x + h*k3), (y + h*j3), (z + h*m3), beta)
    
    x = x + (h/6)*(k1 + 2*k2 + 2*k3 + k4)
    y = y + (h/6)*(j1 + 2*j2 + 2*j3 + j4) 
    z = z + (h/6)*(m1 + 2*m2 + 2*m3 + m4) 
    
    return(x, y, z)

       
rho = 28.0
sigma = 10.0
beta = 8.0 / 3.0 


h = 0.01 #stepsize
n = 4000 #points

x0, y0, z0 = 1.0, 1.0, 1.0

vx = [x0]
vy = [y0]
vz = [z0]

vex = [x0]
vey = [y0]
vez = [z0]

start = time.time()

for i in range(n):
    
    x = vx[i]
    y = vy[i]
    z = vz[i]
    
    x1 = vex[i]
    y1 = vey[i]
    z1 = vez[i]
    position = rk4(x, y, z, sigma, rho, beta, h)
    euler_position = lz_euler(x1,y1,z1,sigma,rho,beta,h)
    
    vx.append(position[0])
    vy.append(position[1])
    vz.append(position[2])
    
    #Euler Points
    vex.append(euler_position[0])
    vey.append(euler_position[1])
    vez.append(euler_position[2])
    
end = time.time()    
print("It took {} seconds to compute Lorenz points. \n".format(end - start))
print("There are {} points displayed".format(n))

# Plot
t = np.arange(len(vx))
# Plot x, y,z in respect to t together to show they are acting unstable

#Euler Plot
plt.figure(figsize =(10,10))
plt.plot(t, vex, 'm', label = 'Rate of Convection')
plt.plot(t, vey, 'g', label = 'Horizontal Temperature Variation')
plt.plot(t, vez, 'b', label = 'Vertical Temperature Variation')
plt.axhline(0, color='k')
plt.axvline(0, color='k')
plt.xlabel('t')
plt.ylabel('x, y, z')
plt.title("Lorenz w euler")
plt.legend()
plt.show() 

#RK4 Plot
plt.figure(figsize =(10,10))
plt.plot(t, vx, 'm', label = 'Rate of Convection')
plt.plot(t, vy, 'g', label = 'Horizontal Temperature Variation')
plt.plot(t, vz, 'b', label = 'Vertical Temperature Variation')
plt.axhline(0, color='k')
plt.axvline(0, color='k')
plt.xlabel('t')
plt.ylabel('x, y, z')
plt.title("Lorenz w rk4")
plt.legend()
plt.show() 

#compare euler lorenz to rk4 lorenz
fig = plt.figure(figsize=(10,10))
ax = Axes3D(fig)

ax.plot(vx,vy,vz, 'k', linewidth=0.5)
ax.plot(vex,vey,vez, 'r', linewidth=0.5)
ax.set_xlabel("Rate of Convection")
ax.set_ylabel("Horizontal Temperature Variation")
ax.set_zlabel("Vertical Temperature Variation")
ax.set_title("Lorenz Attractor")



# Comparing Lorenz Attractors
# Create another seperate Lorenz Attractor to conpare to
# original LA
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
rho1 = 28.0
sigma1 = 10.0
beta1 = 8.0 / 3.0 

vx1 = [x0]
vy1 = [y0]
vz1 = [z0]



for i in range(n):
    x = vx1[i]
    y = vy1[i]
    z = vz1[i]

    position = rk4(x, y, z, sigma1, rho1, beta1, h)

    vx1.append(position[0])
    vy1.append(position[1])
    vz1.append(position[2])

# plotting Lorenz Attractors in full
fig = plt.figure(figsize=(10,10))
ax = Axes3D(fig)

ax.plot(vx,vy,vz, 'k', linewidth=0.5)
ax.plot(vx1,vy1,vz1, 'r', linewidth=0.5)
ax.set_xlabel("Rate of Convection")
ax.set_ylabel("Horizontal Temperature Variation")
ax.set_zlabel("Vertical Temperature Variation")
ax.set_title("Lorenz Attractor")

plt.show()

#Plotting x, y, z in respect to t
plt.figure(figsize = (10,10))
plt.plot(t, vx, 'k', label = 'og Lorenz')
plt.plot(t, vx1, 'm', label = 'new Lorenz')
plt.axhline(0, color='k')
plt.axvline(0, color='k')
plt.xlabel('t')
plt.ylabel('x')
plt.title("Rate of Convection")
plt.legend()
plt.show() 

plt.figure(figsize = (10,10))
plt.plot(t, vy, 'k', label = 'og Lorenz')
plt.plot(t, vy1, 'g', label = 'new Lorenz')
plt.axhline(0, color='k')
plt.axvline(0, color='k')
plt.xlabel('t')
plt.ylabel('y')
plt.title("Horizontal Temperature Variation")
plt.legend()
plt.show() 

plt.figure(figsize = (10,10))
plt.plot(t, vz, 'k', label = 'og Lorenz')
plt.plot(t, vz1, 'b', label = 'new Lorenz')
plt.axhline(0, color='k')
plt.axvline(0, color='k')
plt.xlabel('t')
plt.ylabel('z')
plt.title("Vertical Temperature Variation")
plt.legend()
plt.show() 

# Rossler Attractor
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def rs_dx_dt(y, z):
    return -y-z

def rs_dy_dt(x, y, a):
    return x + a*y

def rs_dz_dt(x, z, b, c):
    return b + z*(x-c)

def rs_euler(x,y,z,a,b,c,h):
    xn = x + h * rs_dx_dt(y,z)
    yn = y + h * rs_dy_dt(x, y, a)
    zn = z + h * rs_dz_dt(x, z, b, c)
    
    return xn, yn, zn

def rs_rk4(x, y, z, a, b, c, h):
    k1 = rs_dx_dt(y, z)
    j1 = rs_dy_dt(x, y, a)
    m1 = rs_dz_dt(x, z, b, c)
    
    k2 = rs_dx_dt((y + h*(j1/2)), (z + h*(m1/2)))
    j2 = rs_dy_dt((x + h*(k1/2)), (y + h*(j1/2)), a)
    m2 = rs_dz_dt((x + h*(k1/2)), (z + h*(m1/2)), b, c)

    k3 = rs_dx_dt((y + h*(j2/2)), (z + h*(m2/2)))
    j3 = rs_dy_dt((x + h*(k2/2)), (y + h*(j2/2)), a)
    m3 = rs_dz_dt((x + h*(k2/2)), (z + h*(m2/2)), b, c)
    
    k4 = rs_dx_dt((y + h*j3), (z + h*m3))
    j4 = rs_dy_dt((x + h*k3), (y + h*j3), a)
    m4 = rs_dz_dt((x + h*k3), (z + h*m3), b, c)
    
    x = x + (h/6)*(k1 + 2*k2 + 2*k3 + k4)
    y = y + (h/6)*(j1 + 2*j2 + 2*j3 + j4) 
    z = z + (h/6)*(m1 + 2*m2 + 2*m3 + m4) 
    
    return(x, y, z)

a = 0.2
b = 0.2
c = 5.7

#a = 0.1
#b = 0.1
#c = 14


h = 0.01 #stepsize
n = 10000 #points

x0, y0, z0 = 0.0, 0.0, 0.0

rs_vx = [x0]
rs_vy = [y0]
rs_vz = [z0]

rs_vex = [x0]
rs_vey = [y0]
rs_vez = [z0]

start = time.time()
for i in range(n):
    x = rs_vx[i]
    y = rs_vy[i]
    z = rs_vz[i]
    x1 = rs_vex[i]
    y1 = rs_vey[i]
    z1 = rs_vez[i]
    
    position = rs_rk4(x, y, z, a, b, c, h)
    euler_position = rs_euler(x1,y1,z1,a,b,c,h)

    rs_vx.append(position[0])
    rs_vy.append(position[1])
    rs_vz.append(position[2])
    #Euler Points
    rs_vex.append(euler_position[0])
    rs_vey.append(euler_position[1])
    rs_vez.append(euler_position[2])
    
end = time.time()    
print("It took {} seconds to compute Rossler points. \n".format(end - start))
print("There are {} points displayed".format(n))

t = np.arange(len(rs_vx))

#Compare Eulers to RK4
plt.figure(figsize =(10,10))
plt.plot(t, rs_vex, 'm', label = 'x')
plt.plot(t, rs_vey, 'g', label = 'y')
plt.plot(t, rs_vez, 'b', label = 'z')
plt.axhline(0, color='k')
plt.axvline(0, color='k')
plt.xlabel('t')
plt.ylabel('x, y, z')
plt.title("Rossler w Euler")
plt.legend()
plt.show() 

# Plot x, y,z in respect to t together to show they are acting unstable
plt.figure(figsize =(10,10))
plt.plot(t, rs_vx, 'm', label = 'x')
plt.plot(t, rs_vy, 'g', label = 'y')
plt.plot(t, rs_vz, 'b', label = 'z')
plt.axhline(0, color='k')
plt.axvline(0, color='k')
plt.xlabel('t')
plt.ylabel('x, y, z')
plt.title("Rossler w RK4")
plt.legend()
plt.show() 



fig = plt.figure(figsize=(10,10))
ax = Axes3D(fig)
ax.plot(rs_vx,rs_vy,rs_vz, 'k', linewidth=0.5)
ax.plot(rs_vex,rs_vey,rs_vez, 'r', linewidth=0.5)
ax.set_xlabel("X Axis")
ax.set_ylabel("Y Axis")
ax.set_zlabel("Z Axis")
ax.set_title("Rossler Attractor")

plt.show()
